#!/usr/bin/env python3

from collections import defaultdict
import django
import argparse
import re

django.setup()

from CIResults.models import Machine  # noqa


def get_machine(name):
    try:
        return Machine.objects.get(name=name)
    except Machine.DoesNotExist:
        return None


def prompt(msg):
    res = input("{} (y/N)".format(msg))
    return res.lower() == "y"


# Parse the options
parser = argparse.ArgumentParser()
parser.add_argument("alias_re", help="Regular expression that needs to match",
                    default=r"^(shard-.*)\d+$", nargs='?')
args = parser.parse_args()

alias_re = re.compile(args.alias_re)

# Look for machines matching the RE and create a shard machine
possible_aliases = defaultdict(list)
for machine in Machine.objects.filter(aliases=None):
    match = alias_re.match(machine.name)
    if match:
        if len(match.groups()) > 1:
            raise ValueError("The regular expression tries to capture more than just one group!")
        possible_aliases[match.groups()[0]].append(machine)

for new_name in possible_aliases:
    new_machine = get_machine(new_name)

    machines = ", ".join([m.name for m in possible_aliases[new_name]])
    if new_machine is None:
        msg = "Would you like to create the machine '{}' and alias the following machines to it? {}"
        if prompt(msg.format(new_name, machines)):
            new_machine = Machine.objects.create(name=new_name, public=False)
        else:
            continue
    else:
        msg = "Would you like to alias the following machines to the existing machine '{}'? {}"
        if not prompt(msg.format(new_name, machines)):
            continue

    for machine in possible_aliases[new_name]:
        machine.aliases = new_machine
        machine.save()
