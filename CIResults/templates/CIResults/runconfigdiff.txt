{% load runconfig_diff %}CI Bug Log - changes from {{diff.runcfg_from}} -> {{diff.runcfg_to}}
====================================================

Summary
-------

  **{{diff.status}}**
{% if diff.status == "SUCCESS" %}
  No regressions found.
{% else %}{% if diff.status == "WARNING" %}
  Minor unknown changes coming with {{diff.runcfg_to}} need to be verified
  manually.
  {% elif diff.status == "FAILURE" %}
  Serious unknown changes coming with {{diff.runcfg_to}} absolutely need to be
  verified manually.
  {% else %}
  Unknown status. This is a bug. Please notice the CI Bug Log developers!
  {% endif %}
  If you think the reported changes have nothing to do with the changes
  introduced in {{diff.runcfg_to}}, please notify your bug team to allow them
  to document this new failure mode, which will reduce false positives in CI.
{% endif %}
  {% if diff.runcfg_to.url|length > 0 %}External URL: {{diff.runcfg_to.url}}{% endif %}

{% if diff.results.new_changes|length > 0 %}Possible new issues
-------------------

  Here are the unknown changes that may have been introduced in {{diff.runcfg_to}}:
{% for testsuite, tests_diffs in diff.results.new_changes.testsuites.items %}
### {{testsuite|show_suppressed}} changes ###

{% if tests_diffs.regressions.tests|length > 0 %}#### Possible regressions ####

  {% for test, machines_diffs in tests_diffs.regressions.compressed.tests.items %}* {{test|show_test}}:{% for result_diff in machines_diffs %}
    - {{result_diff.markdown}}{% endfor %}

  {% endfor %}
{% endif %}{% if tests_diffs.fixes.tests|length > 0 %}#### Possible fixes ####

  {% for test, machines_diffs in tests_diffs.fixes.compressed.tests.items %}* {{test|show_test}}:{% for result_diff in machines_diffs %}
    - {{result_diff.markdown}}{% endfor %}
  {% endfor %}
{% endif %}{% if tests_diffs.warnings.tests|length > 0 %}#### Warnings ####

  {% for test, machines_diffs in tests_diffs.warnings.compressed.tests.items %}* {{test|show_test}}:{% for result_diff in machines_diffs %}
    - {{result_diff.markdown}}{% endfor %}

  {% endfor %}
{% endif %}{% if tests_diffs.suppressed.tests|length > 0 %}#### Suppressed ####

  The following results come from untrusted machines, tests, or statuses.
  They do not affect the overall result.

  {% for test, machines_diffs in tests_diffs.suppressed.compressed.tests.items %}* {{test|show_test}}:{% for result_diff in machines_diffs %}
    - {{result_diff.markdown}}{% endfor %}

  {% endfor %}
{% endif %}{% endfor %}{% endif %}{% if diff.new_tests|length > 0 %}New tests
---------

  New tests have been introduced between {{diff.runcfg_from}} and {{diff.runcfg_to}}:
{% for testsuite, tests_diffs in diff.new_tests.testsuites.items %}
### New {{testsuite|show_suppressed}} tests ({{tests_diffs.tests|length}}) ###

  {% for test, diff_results in tests_diffs.tests.items %}* {{test.name}}:
    - Statuses :{% for status, results_count in diff_results.to_statuses.items %} {{results_count}} {{status.name}}(s){% endfor %}
    - Exec time: {{diff_results.to_exec_times}}

  {% endfor %}

{% endfor %}{% endif %}{% if diff.results.known_changes|length > 0 %}Known issues
------------

  Here are the changes found in {{diff.runcfg_to}} that come from known issues:
{% for testsuite, tests_diffs in diff.results.known_changes.testsuites.items %}
### {{testsuite|show_suppressed}} changes ###

{% if tests_diffs.regressions.tests|length > 0 %}#### Issues hit ####

  {% for test, machines_diffs in tests_diffs.regressions.compressed.tests.items %}* {{test|show_test}}:{% for result_diff in machines_diffs %}
    - {{result_diff.markdown}}{% endfor %}

  {% endfor %}
{% endif %}{% if tests_diffs.fixes.tests|length > 0 %}#### Possible fixes ####

  {% for test, machines_diffs in tests_diffs.fixes.compressed.tests.items %}* {{test|show_test}}:{% for result_diff in machines_diffs %}
    - {{result_diff.markdown}}{% endfor %}

  {% endfor %}
{% endif %}{% if tests_diffs.warnings.tests|length > 0 %}#### Warnings ####

  {% for test, machines_diffs in tests_diffs.warnings.compressed.tests.items %}* {{test|show_test}}:{% for result_diff in machines_diffs %}
    - {{result_diff.markdown}}{% endfor %}

  {% endfor %}
{% endif %}{% if tests_diffs.suppressed.tests|length > 0 %}#### Suppressed ####

  The following results come from untrusted machines, tests, or statuses.
  They do not affect the overall result.

  {% for test, machines_diffs in tests_diffs.suppressed.compressed.tests.items %}* {{test|show_test}}:{% for result_diff in machines_diffs %}
    - {{result_diff.markdown}}{% endfor %}

  {% endfor %}
{% endif %}{% endfor %}{% endif %}{% if diff.results.new_changes|length == 0 and diff.results.known_changes|length == 0 %}
Changes
-------

  No changes found
{% endif %}{% if diff.has_suppressed_results %}  {name}: This element is suppressed. This means it is ignored when computing
          the status of the difference (SUCCESS, WARNING, or FAILURE).

{% endif %}{% for bug in diff.bugs %}  [{{bug.short_name}}]: {{bug.url}}
{% endfor %}

Participating hosts ({{diff.machines.runcfg_from|length}} -> {{diff.machines.runcfg_to|length}})
------------------------------
{% if not diff.has_sufficient_machines %}
  ERROR: It appears as if the changes made in {{diff.runcfg_to}} prevented too many machines from booting.
{% endif %}{% if diff.machines.new|length > 0 or diff.machines.removed|length > 0%}{% if diff.machines.new|length > 0 %}
  Additional ({{diff.machines.new|length}}): {% for m in diff.machines.new %}{{m.name}} {% endfor %}{% endif %}{% if diff.machines.removed|length > 0 %}
  Missing    ({{diff.machines.removed|length}}): {% for m in diff.machines.removed %}{{m.name}} {% endfor %}{% endif %}{% else %}
  No changes in participating hosts{% endif %}


Build changes
-------------

{% for component, c_diff in diff.builds.items %}  * {{component.name}}: {{c_diff.from_build}} -> {{c_diff.to_build}}
{% empty %}  No differences in builds
{% endfor %}
{% for build in diff.builds_all %}  {{build.name}}: {{build.url}}
{% endfor %}
